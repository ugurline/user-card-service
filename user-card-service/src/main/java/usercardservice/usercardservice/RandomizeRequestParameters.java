package usercardservice.usercardservice;

import java.util.Random;

public class RandomizeRequestParameters {
	
	public static String getRandomEmail() {
		
		String email_host = "@gmail.com";
		String email_name = "abc";
		Random rand = new Random();
		int  n = rand.nextInt(999999999) + 100000000;
		
		return (email_name + n + email_host).toString();
		
	}
	
	public static String getRandomLoginName() {
		
		String login_name = "abcd";
		Random rand = new Random();
		int  n = rand.nextInt(999999999) + 100000000;
		
		return (login_name + n ).toString();
		
	}
	
	public static String getRandomPhoneNum() {
		
		Random rand = new Random();
		int  phone_num = rand.nextInt(999999999) + 100000000;
		
		return ("+" + phone_num ).toString();
		
	}

}
