package usercardservice.usercardservice;

import org.json.JSONObject;

public class RegisterUserBodyGenerator {

	public static String getValidBody(String login_name , String email_for_request, String phone_number_for_request)  {
	
					
		JSONObject jsonBody = new JSONObject()
				.put("salutation", "MR" )
				.put("first-name", "John")
				.put("last-name", "Smith")
				.put("birth-date", "1992-09-24")
				.put("mobile-number", phone_number_for_request)
				.put("email", email_for_request)
				.put("login-name", login_name)
				.put("password", "Johnny_1992");
									
		System.out.println(jsonBody);
	
		return jsonBody.toString();
	}
	
	public static String getInvalidBodyWithMissingParamter(String login_name , String phone_number_for_request)  {
		
		
		JSONObject jsonBody = new JSONObject()
				.put("salutation", "MR" )
				.put("first-name", "John")
				.put("last-name", "Smith")
				.put("birth-date", "1992-09-24")
				.put("mobile-number", phone_number_for_request)
				//.put("email", "abc@def.co.de")  REMOVE EMAIL FROM REQUEST
				.put("login-name", login_name)
				.put("password", "Johnny_1992");
									
		System.out.println(jsonBody);
	
		return jsonBody.toString();
	}
	
	

}
