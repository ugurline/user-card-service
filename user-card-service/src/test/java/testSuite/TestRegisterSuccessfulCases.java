package testSuite;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import usercardservice.usercardservice.RandomizeRequestParameters;
import usercardservice.usercardservice.RegisterUserBodyGenerator;

public class TestRegisterSuccessfulCases extends TestBase {

	@BeforeClass
	private void setBaseUrl() {

		
		RestAssured.basePath = "user/";

	}

	@Test
	private void testSuccesfulRequest() {
		String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String email_for_request = RandomizeRequestParameters.getRandomEmail();  
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum();  

		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request );

		RestAssured.given().body(body).param("status", "200").put().then().assertThat().statusCode(200);

		String login_name_in_response = RestAssured.given().body(body).param("status", "200").put().jsonPath()
				.getJsonObject("register-user-response.login-name");

		String status = RestAssured.given().body(body).param("status", "200").put().jsonPath()
				.getJsonObject("register-user-response.status");

		String pan = RestAssured.given().body(body).param("status", "200").put().jsonPath()
				.getJsonObject("register-user-response.pan");

		
		login_name_for_request = "johnsmith1980";// Renaming the login_name because mock service can't be randomized in free version
		assertEquals(login_name_in_response, login_name_for_request);
		assertEquals(status, "ACTIVE");
		assertEquals(pan, "4123456789012345");

		System.out.println("login_name : " + login_name_in_response);
		System.out.println("status : " + status);
		System.out.println("pan : " + pan);

	}

}
