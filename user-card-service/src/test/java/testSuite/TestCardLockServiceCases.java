package testSuite;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import io.restassured.RestAssured;

public class TestCardLockServiceCases extends TestBase{
	
	private String usernameForAuth = "username";
	private String passwordForAuth = "password";

	@BeforeMethod
	private void setBaseUrl() {

		RestAssured.basePath = "user/{login-name}/card/{pan}/lock/{lock-ref-id}";		


	}
	
	
	@Test(priority= 1)
	private void testSuccessfulLocking() {
		
		String body = "{\"card-lock\": {\"locking-reason\": \"lost card\" }}";
		RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012345")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.then()
		.assertThat()
		.statusCode(200);
		
		String message = RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012345")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.getBody()
		.jsonPath()
		.getJsonObject("status");
		
		assertEquals(message, "OK","response message is wrong!");

	}
	
	@Test
	private void testUnSuccessfulLockingForMissingParam() {
		
		String body = "{\"card-lock\": {\"locking-reason\": \"lost card\" }}";
		RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012346")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.then()
		.assertThat()
		.statusCode(400);
		
		String message = RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012346")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.getBody()
		.jsonPath()
		.getJsonObject("status");
		
		assertEquals(message, "MISSING_PARAMETER","response message is wrong!");
	}
	
	@Test
	private void testUnSuccessfulLockingForAlreadyUsedRef() {
		
		String body = "{\"card-lock\": {\"locking-reason\": \"lost card\" }}";
		RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012347")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.then()
		.assertThat()
		.statusCode(403);
		
		String message = RestAssured.given()
		.pathParam("login-name", "JohnSmith")
		.pathParam("pan", "4123456789012347")
		.pathParam("lock-ref-id", "1234")
		.auth().basic(usernameForAuth, passwordForAuth)
		.body(body)
		.put()
		.getBody()
		.jsonPath()
		.getJsonObject("status");
		
		assertEquals(message, "TRANSACTION_ALREADY_USED ","response message is wrong!");
	}
}
