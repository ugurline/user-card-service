package testSuite;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import usercardservice.usercardservice.RandomizeRequestParameters;
import usercardservice.usercardservice.RegisterUserBodyGenerator;

public class TestGetUserCases extends TestBase{
	
	String usernameForAuth = "username";
	String passwordForAuth = "password";
	String pan = "";
	String bad_pan = "4123456789012346";
	String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
	String active_status = "ACTIVE";
	String locked_status = "LOCKED";

	
	@BeforeClass
	private void setBaseUrl() {

				
		pan = registerCustomerForPan();
		

	}
	
	private String registerCustomerForPan() {
		RestAssured.basePath = "user/";
		
		this.login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String email_for_request = RandomizeRequestParameters.getRandomEmail();  
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum();

		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request );

		String pan = RestAssured.given()
		.body(body)
		.param("status", "200")
		.put()
		.getBody()
		.jsonPath()
		.getJsonObject("register-user-response.pan");
		
		

		
		return pan;
	}
	
	@Test
	private void testSuccesfulRequest() {
		
		login_name_for_request = "JohnSmith"; // overwriting username because can't be parametrized in mockable.io free version
		
		System.out.println("pan  = " + pan );
		System.out.println("login_name_for_request  = " + login_name_for_request );
		
		RestAssured.basePath = "user/{login-name}/card/{pan}";		
		RestAssured.given()
		.pathParam("login-name", login_name_for_request)
		.pathParam("pan", pan)
		.auth().basic(usernameForAuth, passwordForAuth)
		.get()
		.then()
		.assertThat()
		.statusCode(200);
		
		String pan_in_response = 		RestAssured.given()
				.pathParam("login-name", login_name_for_request)
				.pathParam("pan", pan)
				.auth().basic(usernameForAuth, passwordForAuth)
				.get()
				.getBody()
				.jsonPath()
				.getJsonObject("card.number");
		
		String status_in_response = 		RestAssured.given()
				.pathParam("login-name", login_name_for_request)
				.pathParam("pan", pan)
				.auth().basic(usernameForAuth, passwordForAuth)
				.get()
				.getBody()
				.jsonPath()
				.getJsonObject("card.status");
		
		
		assertEquals(pan, pan_in_response,"pan value is wrong!");
		assertEquals(active_status, status_in_response,"status value is wrong!");
	}
	
	
	@Test
	private void testUnSuccesfulRequest() {
		
		RestAssured.basePath = "user/{login-name}/card/{pan}";		
		RestAssured.given()
		.pathParam("login-name", login_name_for_request)
		.pathParam("pan", bad_pan)
		.auth().basic(usernameForAuth, passwordForAuth)
		.get()
		.then()
		.assertThat()
		.statusCode(404);
		
		String status_in_response = 		RestAssured.given()
				.pathParam("login-name", login_name_for_request)
				.pathParam("pan", bad_pan)
				.auth().basic(usernameForAuth, passwordForAuth)
				.get()
				.getBody()
				.jsonPath()
				.getJsonObject("card.status");
		
		System.out.println("status_in_response  = " + status_in_response );

		assertEquals(locked_status, status_in_response,"status value is wrong!");

	}
}
