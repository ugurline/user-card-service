package testSuite;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import io.restassured.RestAssured;

public class TestBase {

	@BeforeClass
	public void initialize() {
		
		RestAssured.baseURI = "http://demo2905557.mockable.io/";		

	}

	@AfterClass
	public void tearDown() {
		
	}
}
