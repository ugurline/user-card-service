package testSuite;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import usercardservice.usercardservice.RandomizeRequestParameters;
import usercardservice.usercardservice.RegisterUserBodyGenerator;

public class TestRegisterUnSuccessfulCases extends TestBase{

	String already_existing_email = "";
	String already_existing_username = "";
	String already_existing_phone = "";

	
	@BeforeClass
	private void setBaseUrl() {

		
		RestAssured.basePath = "user/";
		setAlreadyExistingParameters();

	}
	
	
	private void setAlreadyExistingParameters() {
		
		String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String email_for_request = RandomizeRequestParameters.getRandomEmail();  
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum();
		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request );

		RestAssured.given().body(body).param("status", "200").put().then().assertThat().statusCode(200);
		already_existing_email = email_for_request;
		already_existing_username = login_name_for_request;
		already_existing_phone = phone_number_for_request;

		System.out.println("already_existing_email : " + already_existing_email);
		System.out.println("already_existing_username : " + already_existing_username);
		System.out.println("already_existing_phone : " + already_existing_phone);

		
	}

	@Test
	private void testUnSuccesfulRequestforStatus400() {

		String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum();  		
		
		String body = RegisterUserBodyGenerator.getInvalidBodyWithMissingParamter(login_name_for_request,phone_number_for_request );

		RestAssured.given().body(body).param("status", "400").put().then().assertThat().statusCode(400);

		String error_key = RestAssured.given().body(body).param("status", "400").put().jsonPath()
				.getJsonObject("error-response.error.error-key");

		String error_message = RestAssured.given().body(body).param("status", "400").put().jsonPath()
				.getJsonObject("error-response.error.error-message");
		
		assertEquals(error_key, "MISSING_MANDATORY_FIELD");
		assertEquals(error_message, "One or several mandatory fields are missing");

		System.out.println("error_key : " + error_key);
		System.out.println("error_message : " + error_message);
	}
	
	@Test
	private void testUnSuccesfulRequestforStatus403_1() {

		String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String email_for_request = RandomizeRequestParameters.getRandomEmail();  
		String phone_number_for_request = already_existing_phone;  
		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request);

		RestAssured.given().body(body).param("status", "403_1").put().then().assertThat().statusCode(403);

		String error_key = RestAssured.given().body(body).param("status", "403_1").put().jsonPath()
				.getJsonObject("error-response.error.error-key");

		String error_message = RestAssured.given().body(body).param("status", "403_1").put().jsonPath()
				.getJsonObject("error-response.error.error-message");
		
		assertEquals(error_key, "USER_WITH_THIS_PHONE_NUMBER_EXISTS");
		assertEquals(error_message, "User with this mobile phone number already exists");

		System.out.println("error_key : " + error_key);
		System.out.println("error_message : " + error_message);
	}
	
	@Test
	private void testUnSuccesfulRequestforStatus403_2() {

		String login_name_for_request = already_existing_username;
		String email_for_request = RandomizeRequestParameters.getRandomEmail();  
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum();  
		
		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request);

		RestAssured.given().body(body).param("status", "403_2").put().then().assertThat().statusCode(403);

		String error_key = RestAssured.given().body(body).param("status", "403_2").put().jsonPath()
				.getJsonObject("error-response.error.error-key");

		String error_message = RestAssured.given().body(body).param("status", "403_2").put().jsonPath()
				.getJsonObject("error-response.error.error-message");
		
		assertEquals(error_key, "USER_WITH_THIS_LOGIN_NAME_EXISTS");
		assertEquals(error_message, "User with this login name already exists");

		System.out.println("error_key : " + error_key);
		System.out.println("error_message : " + error_message);
	}
	
	@Test
	private void testUnSuccesfulRequestforStatus403_3() {
		
		String login_name_for_request = RandomizeRequestParameters.getRandomLoginName();  
		String email_for_request = already_existing_email; 
		String phone_number_for_request = RandomizeRequestParameters.getRandomPhoneNum(); 

		String body = RegisterUserBodyGenerator.getValidBody(login_name_for_request, email_for_request, phone_number_for_request);

		RestAssured.given().body(body).param("status", "403_3").put().then().assertThat().statusCode(403);

		String error_key = RestAssured.given().body(body).param("status", "403_3").put().jsonPath()
				.getJsonObject("error-response.error.error-key");

		String error_message = RestAssured.given().body(body).param("status", "403_3").put().jsonPath()
				.getJsonObject("error-response.error.error-message");
		
		assertEquals(error_key, "USER_WITH_THIS_EMAIL_EXISTS");
		assertEquals(error_message, "User with this email address already exists");

		System.out.println("error_key : " + error_key);
		System.out.println("error_message : " + error_message);
	}

}
