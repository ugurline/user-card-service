## Synopsis

Purpose of this project is to run automated tests for Get card details and Lock card application.

## Code Example

I believe that best documentation for code is the code itself. So I always try to make it clear and understandable by third parties.
I rarely use comments because I make namings clear.

I have two packages in the project: usercardservice.usercardservice, and testSuite.

Main idea is to keep test classes and others separately.
I have four test classes in order to achieve the goal set in the challenge. I also keep positive and negative scenarios separate classes when needed.
I have one class "RegisterUserBodyGenerator" for generating the json bodies required in the requests.
Other class "RandomizeRequestParameters" is used for passing required arguments randomized to body generator. So that we won't use same cusomters,cards etc.  

For mock data I used www.mockable.io platform. Since free version of this service doesn't allow me to parameterize responses according to requests, I had to use some static values in the requests to correspond the responses.
If I had a real api application, this wouldn't be the case.



## Motivation
Written upon request by Wirecard.

## Installation
This is a maven project, written on Eclipse Version: Oxygen.3a Release (4.7.3a).

Test framework I'm using is TestNG.
For api testing, I'm using Rest Assured.
For JSON parsing/reading , I'm using org.json.
They're all defined as dependencies in pom.xml

I tested it both Windows and Ubuntu machines and seems to be working fine.

**To run the tests you will need maven installed on the machine.**

## Tests
To run the tests, first pull the project to local and while in project folder, go to command line use **"mvn install"** command to get all dependencies.

Then use **"mvn test"** to run the test class.
After tests are finishes, this should be seen on the terminal:

[INFO] Tests run: 10, Failures: 0, Errors: 0, Skipped: 0, Time elapsed: 15.061 s - in TestSuite
[INFO] 
[INFO] Results:
[INFO] 
[INFO] Tests run: 10, Failures: 0, Errors: 0, Skipped: 0



Tester can also run the tests in Eclipse by right clicking TestCommentFunction class -> Run As -> TestNG test

## Contributors
Ugur Aydin
